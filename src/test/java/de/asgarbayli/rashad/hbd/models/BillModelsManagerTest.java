package de.asgarbayli.rashad.hbd.models;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author rashad
 */
public class BillModelsManagerTest {

    public static final String fileName = "BillModels.json";
    File file;

    public BillModelsManagerTest() {
        this.file = new File(fileName);
    }

    @BeforeClass
    public static void setUpClass() {
        LinkedList<BillModel> billModels = new LinkedList<>();

        // Models
        BillModel billModel = new BillModel();
        billModel.setCounterID("TestCounter1");
        billModel.setCounterDescription("Test Counter Descriptor");
        billModel.setStartCounter(Integer.MIN_VALUE);
        billModel.setStartDate(LocalDate.MIN);
        LinkedList<LocalDateMap<Integer>> counterHistory = new LinkedList<>();
        counterHistory.addLast(new LocalDateMap(LocalDate.MIN.plusDays(10L), Integer.MIN_VALUE + 10));
        counterHistory.addLast(new LocalDateMap(LocalDate.MIN.plusDays(20L), Integer.MIN_VALUE + 20));
        billModel.setCounterHistory(counterHistory);
        billModel.setConsumeFactor(Double.MAX_VALUE);
        billModel.setUnitPrice(Double.MIN_NORMAL);
        billModel.setBasePrice(Double.MAX_VALUE);
        billModel.setMonthlyPaymentInAdvance(Double.MAX_VALUE);
        billModel.setBasePrice(Double.MIN_VALUE);
        billModel.setTax(Double.NaN);

        BillModel otherBillModel = new BillModel();
        otherBillModel.setCounterID("TestCounter1");
        otherBillModel.setCounterDescription("Test Counter Descriptor");
        otherBillModel.setStartCounter(Integer.MIN_VALUE);
        otherBillModel.setStartDate(LocalDate.MIN);
        LinkedList<LocalDateMap<Integer>> otherCounterHistory = new LinkedList<>();
        otherCounterHistory.addLast(new LocalDateMap(LocalDate.MIN.plusDays(10L), Integer.MIN_VALUE + 10));
        otherCounterHistory.addLast(new LocalDateMap(LocalDate.MIN.plusDays(20L), Integer.MIN_VALUE + 20));
        otherBillModel.setCounterHistory(otherCounterHistory);
        otherBillModel.setConsumeFactor(Double.MAX_VALUE);
        otherBillModel.setUnitPrice(Double.MIN_NORMAL);
        otherBillModel.setBasePrice(Double.MAX_VALUE);
        otherBillModel.setMonthlyPaymentInAdvance(Double.MAX_VALUE);
        otherBillModel.setBasePrice(Double.MIN_VALUE);
        otherBillModel.setTax(Double.NaN);

        // Put them into the same map
        billModels.addLast(billModel);
        billModels.addLast(otherBillModel);

        // Now write into the file
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(fileName);
        try {
            mapper.writeValue(file, billModels);
        } catch (IOException ex) {
            Logger.getLogger(BillModelsManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @AfterClass
    public static void tearDownClass() {
        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }
    }

    /**
     * Test of loadBillModelsFromLocalFile method, of class BillModelsManager.
     */
    @org.junit.Test
    public void testLoadBillModelsFromLocalFile() {
        System.out.println("loadBillModelsFromLocalFile");

        // There is stored data, read it
        ObjectMapper mapper = new ObjectMapper();
        BillModelsManager billModelsManager = new BillModelsManager();
        try {
            billModelsManager.setBillModels((LinkedList<BillModel>) mapper.readValue(file, new TypeReference<LinkedList<BillModel>>() {
            }));
        } catch (IOException ex) {
            Logger.getLogger(BillModelsManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertTrue(!billModelsManager.getBillModels().isEmpty());
        BillModel billModel = billModelsManager.getBillModels().pollFirst();
        assertTrue(billModel != null);
        assertTrue(!billModel.getCounterHistory().isEmpty());
        assertEquals(LocalDate.MIN, billModel.getStartDate());
        // TODO Extend the checks
    }

    /**
     * Test of storeBillModelsIntoLocalFile method, of class BillModelsManager.
     */
    @org.junit.Test
    public void testStoreBillModelsIntoLocalFile() {
        System.out.println("storeBillModelsIntoLocalFile");
        if (file.exists()) {
            file.delete();
        }
        assertTrue(!file.exists());

        // Models
        BillModel billModel = new BillModel();
        billModel.setCounterID("TestCounter1");
        billModel.setCounterDescription("Test Counter Descriptor");
        billModel.setStartCounter(Integer.MIN_VALUE);
        billModel.setStartDate(LocalDate.MIN);
        LinkedList<LocalDateMap<Integer>> counterHistory = new LinkedList<>();
        counterHistory.addLast(new LocalDateMap(LocalDate.MIN.plusDays(10L), Integer.MIN_VALUE + 10));
        counterHistory.addLast(new LocalDateMap(LocalDate.MIN.plusDays(20L), Integer.MIN_VALUE + 20));
        billModel.setCounterHistory(counterHistory);
        billModel.setConsumeFactor(Double.MAX_VALUE);
        billModel.setUnitPrice(Double.MIN_NORMAL);
        billModel.setBasePrice(Double.MAX_VALUE);
        billModel.setMonthlyPaymentInAdvance(Double.MAX_VALUE);
        billModel.setBasePrice(Double.MIN_VALUE);
        billModel.setTax(Double.NaN);

        BillModelsManager instance = new BillModelsManager();
        instance.getBillModels().addLast(billModel);
        instance.storeBillModelsIntoLocalFile();
        assertTrue(file.exists());
        // TODO Extend the checks
    }

    /**
     * Test of getBillModels method, of class BillModelsManager.
     */
    @org.junit.Test
    public void testGetBillModels() {
        System.out.println("getBillModels");
        BillModelsManager instance = new BillModelsManager();
        LinkedList<BillModel> expResult = new LinkedList<>();
        expResult.add(new BillModel());
        expResult.add(new BillModel());
        instance.setBillModels(expResult);
        LinkedList<BillModel> result = instance.getBillModels();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBillModels method, of class BillModelsManager.
     */
    @org.junit.Test
    public void testSetBillModels() {
        System.out.println("setBillModels");
        BillModelsManager instance = new BillModelsManager();
        LinkedList<BillModel> expResult = new LinkedList<>();
        expResult.add(new BillModel());
        expResult.add(new BillModel());
        instance.setBillModels(expResult);
        LinkedList<BillModel> result = instance.getBillModels();
        assertEquals(expResult, result);
    }
}
