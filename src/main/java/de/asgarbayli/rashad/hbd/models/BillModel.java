package de.asgarbayli.rashad.hbd.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Objects;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

/**
 * This class represents a universal bill model, that can be used for any type
 * of bill (counter of energy consume).
 *
 * @author Rashad Asgarbayli
 * @version 1.0
 */
public class BillModel {
    // Counter Identity

    @NotNull
    @NotEmpty
    private String counterID;

    private String counterDescription;

    // Counters
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate startDate;

    @NotNull
    @NumberFormat
    private Integer startCounter;

    private LinkedList<LocalDateMap<Integer>> counterHistory;

    // Factor applied to the counter difference
    @NumberFormat
    private Double consumeFactor;

    // Prices & Payments
    @NotNull
    @NumberFormat
    private Double unitPrice;

    @NotNull
    @NumberFormat
    private Double basePrice;

    @NotNull
    @NumberFormat
    private Double monthlyPaymentInAdvance;

    @NotNull
    @NumberFormat
    private Double tax;

    public BillModel() {
        // There is no need for an explicite implementation.
        /*
        this.counterID = "ID12345";
        this.startDate = LocalDate.now().toString();
        this.startCounter = 0;
        this.unitPrice = 1.0;
        this.basePrice = 0.0;
        this.monthlyPaymentInAdvance = 0.0;
        this.tax = 17.5;
         */
    }

    /**
     * @return the counterID
     */
    public String getCounterID() {
        return counterID;
    }

    /**
     * @param counterID the counterID to set
     */
    public void setCounterID(String counterID) {
        this.counterID = counterID;
    }

    /**
     * @return the counterDescription
     */
    public String getCounterDescription() {
        return counterDescription;
    }

    /**
     * @param counterDescription the counterDescription to set
     */
    public void setCounterDescription(String counterDescription) {
        this.counterDescription = counterDescription;
    }

    /**
     * @return the startCounter
     */
    public Integer getStartCounter() {
        return startCounter;
    }

    /**
     * @param startCounter the startCounter to set
     */
    public void setStartCounter(Integer startCounter) {
        this.startCounter = startCounter;
    }

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the counterHistory
     */
    public LinkedList<LocalDateMap<Integer>> getCounterHistory() {
        return counterHistory;
    }

    /**
     * @param counterHistory the counterHistory to set
     */
    public void setCounterHistory(LinkedList<LocalDateMap<Integer>> counterHistory) {
        this.counterHistory = counterHistory;
    }

    /**
     * @return the consumeFactor
     */
    public Double getConsumeFactor() {
        return consumeFactor;
    }

    /**
     * @param consumeFactor the consumeFactor to set
     */
    public void setConsumeFactor(Double consumeFactor) {
        this.consumeFactor = consumeFactor;
    }

    /**
     * @return the unitPrice
     */
    public Double getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the basePrice
     */
    public Double getBasePrice() {
        return basePrice;
    }

    /**
     * @param basePrice the basePrice to set
     */
    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    /**
     * @return the monthlyPaymentInAdvance
     */
    public Double getMonthlyPaymentInAdvance() {
        return monthlyPaymentInAdvance;
    }

    /**
     * @param monthlyPaymentInAdvance the monthlyPaymentInAdvance to set
     */
    public void setMonthlyPaymentInAdvance(Double monthlyPaymentInAdvance) {
        this.monthlyPaymentInAdvance = monthlyPaymentInAdvance;
    }

    /**
     * @return the tax
     */
    public Double getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(Double tax) {
        this.tax = tax;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BillModel other = (BillModel) obj;
        return Objects.equals(this.counterID, other.counterID);
    }
}
