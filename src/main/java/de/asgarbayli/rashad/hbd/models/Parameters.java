/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.asgarbayli.rashad.hbd.models;

import de.asgarbayli.rashad.hbd.controllers.definitions.Navigation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default interface to read and write settings.
 * Creates Java properties file. Saves settings in it. Reads Saved settings from it.
 * @author rashad
 */
public class Parameters {
    private LocalDate contractDate;
    private double contractVAT;
    private long counterElectricity;
    private long counterGas;
    private double advanceElectricity;
    private double baseElectricity;
    private double unitPriceElectricity;
    private double advanceGas;
    private double baseGas;
    private double unitPriceGas;
    private double conditionGas;
    private double calorificationGas;
    
    Properties settings;
    private boolean empty;
    File file;
    
    /**
     * Default constructor of the class that initialises contractDate with today and all others with zeros.
     * Then calls loadParameters() to read saved settings if there are.
     */
    public Parameters() {
        contractDate = LocalDate.now();
        contractVAT = 0.0;
        counterElectricity = 0;
        counterGas = 0;
        advanceElectricity = 0.0;
        baseElectricity = 0.0;
        unitPriceElectricity = 0.0;
        advanceGas = 0.0;
        baseGas = 0.0;
        unitPriceGas = 0.0;
        conditionGas = 0.0;
        calorificationGas = 0.0;

        settings = new Properties();
        empty = true;
        file = new File(ParameterProperties.FILE_NAME);
        
        loadParameters();
    }

    /**
     * Loads calculation parameters from the defined file.
     */
    public final void loadParameters() {
        if (file.exists()) {
            empty = false;
        }
        InputStream is;
        try {
            is = new FileInputStream(file);

            settings.load(is);

            contractDate = LocalDate.parse(settings.getProperty(ParameterProperties.CONTRACT_DATE, "2016-11-08"));
            setContractVAT(Double.parseDouble(settings.getProperty(ParameterProperties.VAT, "0.0")));
            counterElectricity = Long.parseLong(settings.getProperty(ParameterProperties.ELEC_COUNTER, "0L"));
            advanceElectricity = Double.parseDouble(settings.getProperty(ParameterProperties.ELEC_MONTHLY, "66.00"));
            baseElectricity = Double.parseDouble(settings.getProperty(ParameterProperties.ELEC_BASE_PRICE, "87.0"));
            unitPriceElectricity = Double.parseDouble(settings.getProperty(ParameterProperties.ELEC_UNIT_PRICE, "0.21"));
            counterGas = Long.parseLong(settings.getProperty(ParameterProperties.GAS_COUNTER, "0L"));
            advanceGas = Double.parseDouble(settings.getProperty(ParameterProperties.GAS_MONTHLY, "41.00"));
            baseGas = Double.parseDouble(settings.getProperty(ParameterProperties.GAS_BASE_PRICE, "108.00"));
            unitPriceGas = Double.parseDouble(settings.getProperty(ParameterProperties.GAS_UNIT_PRICE, "0.047"));
            conditionGas = Double.parseDouble(settings.getProperty(ParameterProperties.GAS_CONDITION_NUMBER, "0.9571"));
            calorificationGas = Double.parseDouble(settings.getProperty(ParameterProperties.GAS_CALORIFICATION_VALUE, "11.393"));

            is.close();
        } catch (IOException ex) {
            empty = true;
        }
    }

    /**
     * Saves calculation parameters into the defined file and then forwards to the next page.
     * @return String that represents the next page.
     */
    public String saveParameters() {
        OutputStream os;
        try {
            os = new FileOutputStream(file);
            settings.setProperty(ParameterProperties.CONTRACT_DATE, contractDate.toString());
            settings.setProperty(ParameterProperties.VAT, Double.toString(getContractVAT()));
            settings.setProperty(ParameterProperties.ELEC_COUNTER, Long.toString(counterElectricity));
            settings.setProperty(ParameterProperties.ELEC_MONTHLY, Double.toString(advanceElectricity));
            settings.setProperty(ParameterProperties.ELEC_BASE_PRICE, Double.toString(baseElectricity));
            settings.setProperty(ParameterProperties.ELEC_UNIT_PRICE, Double.toString(unitPriceElectricity));
            settings.setProperty(ParameterProperties.GAS_COUNTER, Long.toString(counterGas));
            settings.setProperty(ParameterProperties.GAS_MONTHLY, Double.toString(advanceGas));
            settings.setProperty(ParameterProperties.GAS_BASE_PRICE, Double.toString(baseGas));
            settings.setProperty(ParameterProperties.GAS_UNIT_PRICE, Double.toString(unitPriceGas));
            settings.setProperty(ParameterProperties.GAS_CONDITION_NUMBER, Double.toString(conditionGas));
            settings.setProperty(ParameterProperties.GAS_CALORIFICATION_VALUE, Double.toString(calorificationGas));

            settings.store(os, null);
            
            os.close();
        } catch (IOException ex) {
            Logger.getLogger(Parameters.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Forward to the home after saving parameters
        return Navigation.DASHBOARD;
    }
    
    /**
     * @return the contractDate
     */
    public LocalDate getContractDate() {
        return contractDate;
    }

    /**
     * @param contractDate the contractDate to set
     */
    public void setContractDate(LocalDate contractDate) {
        this.contractDate = contractDate == null ? LocalDate.now() : contractDate;
    }

    /**
     * @return the counterElectricity
     */
    public long getCounterElectricity() {
        return counterElectricity;
    }

    /**
     * @param counterElectricity the counterElectricity to set
     */
    public void setCounterElectricity(long counterElectricity) {
        this.counterElectricity = Math.max(0L, counterElectricity);
    }

    /**
     * @return the counterGas
     */
    public long getCounterGas() {
        return counterGas;
    }

    /**
     * @param counterGas the counterGas to set
     */
    public void setCounterGas(long counterGas) {
        this.counterGas = Math.max(0L, counterGas);
    }

    /**
     * @return advanceElectricity
     */
    public double getAdvanceElectricity() {
        return advanceElectricity;
    }

    /**
     * @param advanceElectricity the advanceElectricity to set
     */
    public void setAdvanceElectricity(double advanceElectricity) {
        this.advanceElectricity = Math.max(0.0, advanceElectricity);
    }

    /**
     * @return the baseElectricity
     */
    public double getBaseElectricity() {
        return baseElectricity;
    }

    /**
     * @param baseElectricity the baseElectricity to set
     */
    public void setBaseElectricity(double baseElectricity) {
        this.baseElectricity = Math.max(0.0, baseElectricity);
    }

    /**
     * @return the unitPriceElectricity
     */
    public double getUnitPriceElectricity() {
        return unitPriceElectricity;
    }

    /**
     * @param unitPriceElectricity the unitPriceElectricity to set
     */
    public void setUnitPriceElectricity(double unitPriceElectricity) {
        this.unitPriceElectricity = Math.max(0.0, unitPriceElectricity);
    }

    /**
     * @return the advanceGas
     */
    public double getAdvanceGas() {
        return advanceGas;
    }

    /**
     * @param advanceGas the advanceGas to set
     */
    public void setAdvanceGas(double advanceGas) {
        this.advanceGas = Math.max(0.0, advanceGas);
    }

    /**
     * @return the baseGas
     */
    public double getBaseGas() {
        return baseGas;
    }

    /**
     * @param baseGas the baseGas to set
     */
    public void setBaseGas(double baseGas) {
        this.baseGas = Math.max(0.0, baseGas);
    }

    /**
     * @return the unitPriceGas
     */
    public double getUnitPriceGas() {
        return unitPriceGas;
    }

    /**
     * @param unitPriceGas the unitPriceGas to set
     */
    public void setUnitPriceGas(double unitPriceGas) {
        this.unitPriceGas = Math.max(0.0, unitPriceGas);
    }

    /**
     * @return the conditionGas
     */
    public double getConditionGas() {
        return conditionGas;
    }

    /**
     * @param conditionGas the conditionGas to set
     */
    public void setConditionGas(double conditionGas) {
        this.conditionGas = Math.max(0.0, conditionGas);
    }

    /**
     * @return the calorificationGas
     */
    public double getCalorificationGas() {
        return calorificationGas;
    }

    /**
     * @param calorificationGas the calorificationGas to set
     */
    public void setCalorificationGas(double calorificationGas) {
        this.calorificationGas = Math.max(0.0, calorificationGas);
    }

    /**
     * @return the empty
     */
    public boolean isEmpty() {
        return empty;
    }

    /**
     * @return the contractVAT
     */
    public double getContractVAT() {
        return contractVAT;
    }

    /**
     * @param contractVAT the contractVAT to set
     */
    public void setContractVAT(double contractVAT) {
        this.contractVAT = contractVAT;
    }
}
