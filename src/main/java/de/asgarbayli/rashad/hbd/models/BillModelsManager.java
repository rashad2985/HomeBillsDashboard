/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.asgarbayli.rashad.hbd.models;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class manages the storing/retrieving the BillModels in local storage.
 * @author Rashad Asgarbayli
 * @version 1.0
 */
public class BillModelsManager {
    private final String fileName = "BillModels.json";
    private LinkedList<BillModel> billModels;

    public BillModelsManager() {
        this.billModels = new LinkedList<>();
    }
    
    /**
     * Loads the stored BillModels from the local file.
     */
    public void loadBillModelsFromLocalFile() {
        File file = new File(fileName);
        if (!file.exists()) {
            // There is nothing to load and process.
            return;
        }
        // There is stored data, read it
        ObjectMapper mapper = new ObjectMapper();
        try {
            this.billModels = mapper.readValue(file, new TypeReference<LinkedList<BillModel>>(){});
        } catch (IOException ex) {
            Logger.getLogger(BillModelsManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void storeBillModelsIntoLocalFile() {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(fileName);
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, billModels);
        } catch (IOException ex) {
            Logger.getLogger(BillModelsManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public BillModel getBillModelUsingCounterID(String contractID) {
        for (BillModel billModel : billModels) {
            if (billModel.getCounterID().equalsIgnoreCase(contractID)) {
                return billModel;
            }
        }
        return null;
    }

    /**
     * @return the billModels
     */
    public LinkedList<BillModel> getBillModels() {
        return billModels;
    }

    /**
     * @param billModels the billModels to set
     */
    public void setBillModels(LinkedList<BillModel> billModels) {
        this.billModels = billModels;
    }
}
