package de.asgarbayli.rashad.hbd.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.time.LocalDate;
import java.util.Comparator;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Just key simple tuple implementation that contains only LocalDate as key and Generic V as value;
 * @author Rashad Asgarbayli
 * @version 1.0
 */
public class LocalDateMap<V> implements Comparable<LocalDateMap>, Comparator<LocalDateMap> {
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate key;
    
    private V value;

    public LocalDateMap() {
    }
    
    public LocalDateMap(LocalDate date, V value) {
        this.key = date;
        this.value = value;
    }
    
    /**
     * @return the key
     */
    public LocalDate getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(LocalDate key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public V getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(V value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        return this.key.equals(o);
    }

    @Override
    public int compareTo(LocalDateMap o) {
        return this.key.compareTo(o.getKey());
    }

    @Override
    public int compare(LocalDateMap ldm1, LocalDateMap ldm2) {
        return Long.compare(ldm1.getKey().toEpochDay(), ldm2.getKey().toEpochDay());
    }
}
