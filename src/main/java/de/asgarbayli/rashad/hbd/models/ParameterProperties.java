/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.asgarbayli.rashad.hbd.models;

/**
 * This class contains the keys for getting/setting from/into Properties file.
 * @author rashad
 */
public class ParameterProperties {
    /**
     * Contains the name of the file to save settings
     */
    public static final String FILE_NAME = "settings.properties";
    
    // COMMON
    /**
     * Key in settings that is associated to the start date of the contract
     */
    public static final String CONTRACT_DATE = "contract_date";
    
    /**
     * Key in settings that is associated to the Value Added Taxes value for the whole contract
     */
    public static final String VAT = "contract_VAT";
    
    // ELECTRICITY
    /**
     * Key in settings that is associated to the start counter of the electricity
     */
    public static final String ELEC_COUNTER = "elec_counter";
    
    /**
     * Key in settings that is associated to the value of the monthly billing for electricity in advance
     */
    public static final String ELEC_MONTHLY = "elec_monthly";
    
    /**
     * Key in settings that is associated to the value of the annual base price for the electricity
     */
    public static final String ELEC_BASE_PRICE = "elec_base";
    
    /**
     * Key in settings that is associated to the value of the unit price for electricity
     */
    public static final String ELEC_UNIT_PRICE = "elec_unit_price";
    
    // GAS
    /**
     * Key in settings that is associated to the start counter of the gas
     */
    public static final String GAS_COUNTER = "gas_counter";
    
    /**
     * Key in settings that is associated to the value of the monthly billing for gas in advance
     */
    public static final String GAS_MONTHLY = "gas_monthly";
    
    /**
     * Key in settings that is associated to the value of the annual base price for the gas
     */
    public static final String GAS_BASE_PRICE = "gas_base";
    
    /**
     * Key in settings that is associated to the value of the unit price for gas
     */
    public static final String GAS_UNIT_PRICE = "gas_unit_price";
    
    /**
     * Key in settings that is associated to the value of the condition factor for converting cubic meter to kW
     */
    public static final String GAS_CONDITION_NUMBER = "gas_condition";
    
    /**
     * Key in settings that is associated to the value of the calorification value for converting cubic meter to kW
     */
    public static final String GAS_CALORIFICATION_VALUE = "gas_calorification";
}
