package de.asgarbayli.rashad.hbd.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.time.LocalDate;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

/**
 * Container to transfer Counter ID, Counter and Date.
 * @author Rashad Asgarbayli
 * @version 1.0
 */
public class Update {
    @NotNull
    @NotEmpty
    private String counterID;
    
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate counterDate;
    
    @NotNull
    @NumberFormat
    private Integer counter;

    /**
     * @return the counterID
     */
    public String getCounterID() {
        return counterID;
    }

    /**
     * @param counterID the counterID to set
     */
    public void setCounterID(String counterID) {
        this.counterID = counterID;
    }

    /**
     * @return the counterDate
     */
    public LocalDate getCounterDate() {
        return counterDate;
    }

    /**
     * @param counterDate the counterDate to set
     */
    public void setCounterDate(LocalDate counterDate) {
        this.counterDate = counterDate;
    }

    /**
     * @return the counter
     */
    public Integer getCounter() {
        return counter;
    }

    /**
     * @param counter the counter to set
     */
    public void setCounter(Integer counter) {
        this.counter = counter;
    }
}
