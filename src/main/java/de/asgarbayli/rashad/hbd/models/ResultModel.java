package de.asgarbayli.rashad.hbd.models;

import java.time.LocalDate;
import java.util.LinkedList;

/**
 * This class contains the calculation results for a bill model.
 * @author Rashad Asgarbayli
 * @version 1.0
 */
public class ResultModel {
    
    private LocalDate startDate;
    private LocalDate endDate;
    private Long days;
    private Double consume;
    private Double totalNow;
    private Double totalExpected;
    
    private Double totalDifference;
    private Double totalLimit;
    
    private LinkedList<LocalDateMap<Integer>> monthlyConsumesInUnit;
    private LinkedList<LocalDateMap<Double>> monthlyConsumesInMoney;

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the consume
     */
    public Double getConsume() {
        return consume;
    }

    /**
     * @param consume the consume to set
     */
    public void setConsume(Double consume) {
        this.consume = consume;
    }

    /**
     * @return the days
     */
    public Long getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(Long days) {
        this.days = days;
    }

    /**
     * @return the totalNow
     */
    public Double getTotalNow() {
        return totalNow;
    }

    /**
     * @param totalNow the totalNow to set
     */
    public void setTotalNow(Double totalNow) {
        this.totalNow = totalNow;
    }

    /**
     * @return the totalExpected
     */
    public Double getTotalExpected() {
        return totalExpected;
    }

    /**
     * @param totalExpected the totalExpected to set
     */
    public void setTotalExpected(Double totalExpected) {
        this.totalExpected = totalExpected;
    }

    /**
     * @return the totalDifference
     */
    public Double getTotalDifference() {
        return totalDifference;
    }

    /**
     * @param totalDifference the totalDifference to set
     */
    public void setTotalDifference(Double totalDifference) {
        this.totalDifference = totalDifference;
    }

    /**
     * @return the totalLimit
     */
    public Double getTotalLimit() {
        return totalLimit;
    }

    /**
     * @param totalLimit the totalLimit to set
     */
    public void setTotalLimit(Double totalLimit) {
        this.totalLimit = totalLimit;
    }

    /**
     * @return the monthlyConsumesInUnit
     */
    public LinkedList<LocalDateMap<Integer>> getMonthlyConsumesInUnit() {
        return monthlyConsumesInUnit;
    }

    /**
     * @param monthlyConsumesInUnit the monthlyConsumesInUnit to set
     */
    public void setMonthlyConsumesInUnit(LinkedList<LocalDateMap<Integer>> monthlyConsumesInUnit) {
        this.monthlyConsumesInUnit = monthlyConsumesInUnit;
    }

    /**
     * @return the monthlyConsumesInMoney
     */
    public LinkedList<LocalDateMap<Double>> getMonthlyConsumesInMoney() {
        return monthlyConsumesInMoney;
    }

    /**
     * @param monthlyConsumesInMoney the monthlyConsumesInMoney to set
     */
    public void setMonthlyConsumesInMoney(LinkedList<LocalDateMap<Double>> monthlyConsumesInMoney) {
        this.monthlyConsumesInMoney = monthlyConsumesInMoney;
    }
}
