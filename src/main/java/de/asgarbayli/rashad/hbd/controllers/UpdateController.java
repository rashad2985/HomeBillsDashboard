package de.asgarbayli.rashad.hbd.controllers;

import de.asgarbayli.rashad.hbd.controllers.definitions.Messages;
import de.asgarbayli.rashad.hbd.controllers.definitions.Navigation;
import de.asgarbayli.rashad.hbd.models.BillModel;
import de.asgarbayli.rashad.hbd.models.BillModelsManager;
import de.asgarbayli.rashad.hbd.models.LocalDateMap;
import de.asgarbayli.rashad.hbd.models.Update;
import java.util.Collections;
import java.util.LinkedList;
import org.javatuples.Pair;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller that handles the GET/POST request for adding counters
 *
 * @author Rashad Asgarbayli
 * @version 1.0
 */
@Controller
public class UpdateController {

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("title", "Update Bill Counters");
        Messages.resetMessage(model);
    }

    @RequestMapping(value = Navigation.UPDATE_COUNTER, method = RequestMethod.GET)
    public ModelAndView showUpdatableCounters(@ModelAttribute(Navigation.UPDATE_COUNTER) Update update, BindingResult result, Model model) {
        // Check for binding errors
        if (result.hasErrors()) {
            String message = "Binding result on " + Navigation.UPDATE_COUNTER + " has error(s) for the GET request method.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.UPDATE_COUNTER);
        }

        model.addAttribute("models", generateSelectionOptions());

        return new ModelAndView(Navigation.UPDATE_COUNTER);
    }

    @RequestMapping(value = Navigation.UPDATE_COUNTER, method = RequestMethod.POST)
    public ModelAndView updateCounter(@ModelAttribute(Navigation.UPDATE_COUNTER) Update update, BindingResult result, Model model) {
        // Check received data
        if (result.hasErrors()) {
            String message = "Binding result on " + Navigation.UPDATE_COUNTER + " has error(s) for the POST request method.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.UPDATE_COUNTER);
        }

        // Refresh the selection list
        model.addAttribute("models", generateSelectionOptions());

        // Now check the received data
        BillModelsManager manager = new BillModelsManager();
        manager.loadBillModelsFromLocalFile();
        BillModel billModel = manager.getBillModelUsingCounterID(update.getCounterID());
        if (billModel == null) {
            // There is no such a model or something gone wrong
            String message = "Bill Model with Counter ID \"" + update.getCounterID() + "\" does not exist.";
            Messages.showErrorMessage(model, message);
        } else {
            if (billModel.getCounterHistory() == null) {
                billModel.setCounterHistory(new LinkedList<LocalDateMap<Integer>>());
            }
            billModel.getCounterHistory().addLast(new LocalDateMap<>(update.getCounterDate(), update.getCounter()));
            Collections.sort(billModel.getCounterHistory());
            manager.storeBillModelsIntoLocalFile();
            String message = "Counter history of the bill model with Counter ID \"" + update.getCounterID() + "\" was updated.";
            Messages.showSuccessMessage(model, message);
        }

        return new ModelAndView(Navigation.UPDATE_COUNTER, Navigation.UPDATE_COUNTER, new Update());
    }

    @RequestMapping(value = Navigation.UPDATE_COUNTER_HISTORY, method = RequestMethod.GET)
    public ModelAndView showCounterHistory(@RequestParam("modelCounterID") String modelCounterID, @ModelAttribute("billModel") BillModel billModel, BindingResult result, Model model) {
        // Check for binding errors
        if (result.hasErrors()) {
            String message = "Binding result on " + Navigation.UPDATE_COUNTER_HISTORY + " has error(s) for the GET request method.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.UPDATE_COUNTER_HISTORY);
        }

        // Check received data
        if (modelCounterID == null || modelCounterID.equalsIgnoreCase("")) {
            String message = "Needed parameter was not passed to the " + Navigation.UPDATE_COUNTER_HISTORY;
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.UPDATE_COUNTER_HISTORY);
        }

        // Search BillModel to list its counter history
        BillModelsManager billModelsManager = new BillModelsManager();
        billModelsManager.loadBillModelsFromLocalFile();
        billModel = billModelsManager.getBillModelUsingCounterID(modelCounterID);
        if (billModel == null) {
            String message = "Bill model with Counter ID \"" + modelCounterID + "\" could not be found.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.UPDATE_COUNTER_HISTORY);
        }
        // Everthing is set up, show!
        return new ModelAndView(Navigation.UPDATE_COUNTER_HISTORY, "billModel", billModel);
    }

    @RequestMapping(value = Navigation.UPDATE_COUNTER_HISTORY, method = RequestMethod.POST)
    public ModelAndView updateCounterHistory(@ModelAttribute("billModel") BillModel billModel, BindingResult result, Model model) {
        // Check for binding errors
        if (result.hasErrors()) {
            String message = "Binding result on " + Navigation.UPDATE_COUNTER_HISTORY + " has error(s) for the POST request method.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.UPDATE_COUNTER_HISTORY);
        }

        // Search BillModel to list its counter history
        BillModelsManager billModelsManager = new BillModelsManager();
        billModelsManager.loadBillModelsFromLocalFile();
        BillModel billModelInFile = billModelsManager.getBillModelUsingCounterID(billModel.getCounterID());
        if (billModelInFile == null) {
            String message = "Bill model with Counter ID \"" + billModel.getCounterID() + "\" could not be found.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.UPDATE_COUNTER_HISTORY);
        }
        // Everything is OK, exchange the list with new one and then save it into the file
        billModelInFile.setCounterHistory(billModel.getCounterHistory());
        billModelsManager.storeBillModelsIntoLocalFile();
        String message = "Counter history of bill model with counter ID \"" + billModel.getCounterID() + "\" was updated.";
        Messages.showSuccessMessage(model, message);
        return new ModelAndView(Navigation.UPDATE_COUNTER_HISTORY, "billModel", billModel);
    }

    private LinkedList<Pair<String, String>> generateSelectionOptions() {
        BillModelsManager manager = new BillModelsManager();
        manager.loadBillModelsFromLocalFile();
        LinkedList<Pair<String, String>> selectionOptions = new LinkedList<>();
        for (BillModel billModel : manager.getBillModels()) {
            selectionOptions.addLast(new Pair<>(billModel.getCounterID(), billModel.getCounterDescription()));
        }
        return selectionOptions;
    }
}
