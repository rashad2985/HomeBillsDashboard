package de.asgarbayli.rashad.hbd.controllers;

import de.asgarbayli.rashad.hbd.controllers.definitions.Messages;
import de.asgarbayli.rashad.hbd.controllers.definitions.Navigation;
import de.asgarbayli.rashad.hbd.models.BillModel;
import de.asgarbayli.rashad.hbd.models.BillModelsManager;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for the BillModelsManager page.
 *
 * @author Rashad Asgarbayli
 * @version 1.0
 */
@Controller
public class BillModelsManagerController {

    public void setTitleOfModelManagement(Model model) {
        model.addAttribute("title", "Manage bills");
        //Messages.resetMessage(model);
    }

    public void setTitleOfModelAdd(Model model) {
        model.addAttribute("title", "Add new bill");
        Messages.resetMessage(model);
    }

    public void setTitleOfModelRemove(Model model) {
        model.addAttribute("title", "Remove bill");
        Messages.resetMessage(model);
    }

    @RequestMapping(value = Navigation.BILL_MODELS, method = RequestMethod.GET)
    public ModelAndView showBillModels(@ModelAttribute("billModels") BillModelsManager billModels, BindingResult result, Model model) {
        // Page Titel
        setTitleOfModelManagement(model);

        // Check for binding errors
        if (result.hasErrors()) {
            String message = "Binding result on " + Navigation.BILL_MODELS + " has error(s) for the GET request method.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.BILL_MODELS);
        }

        billModels.loadBillModelsFromLocalFile();
        if (billModels.getBillModels().isEmpty()) {
            model.addAttribute("emptyList", true);
        }
        return new ModelAndView(Navigation.BILL_MODELS, "command", billModels);
    }

    @RequestMapping(value = Navigation.BILL_MODELS, method = RequestMethod.POST)
    public ModelAndView changeParameteres(@ModelAttribute("billModels") BillModelsManager billModels, BindingResult result, Model model) {
        // Page Titel
        setTitleOfModelManagement(model);

        // Check received data
        if (result.hasErrors()) {
            String message = "Binding result on " + Navigation.BILL_MODELS + " has error(s) for the POST request method.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.BILL_MODELS);
        }

        // Process the received data
        billModels.storeBillModelsIntoLocalFile();
        billModels.loadBillModelsFromLocalFile();

        // Log
        String message = "Operation was successfull.";
        Messages.showSuccessMessage(model, message);

        // Done, reload and show on the same page
        return new ModelAndView(Navigation.BILL_MODELS, "billModels", billModels);
    }

    @RequestMapping(value = Navigation.BILL_MODELS_ADD, method = RequestMethod.GET)
    public ModelAndView showForm(@ModelAttribute(Navigation.BILL_MODELS_ADD) BillModel billModel, BindingResult result, Model model) {
        // Page Titel
        setTitleOfModelAdd(model);

        // Check received data
        if (result.hasErrors()) {
            String message = "Binding result on " + Navigation.BILL_MODELS_ADD + " has error(s) for the GET request method.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.BILL_MODELS_ADD);
        }

        billModel = new BillModel();
        return new ModelAndView(Navigation.BILL_MODELS_ADD, "command", billModel);
    }

    @RequestMapping(value = Navigation.BILL_MODELS_ADD, method = RequestMethod.POST)
    public ModelAndView addModel(@ModelAttribute(Navigation.BILL_MODELS_ADD) @Valid BillModel billModel, BindingResult result, Model model) {
        // Page Titel
        setTitleOfModelAdd(model);

        // Check received data
        if (result.hasErrors()) {
            String message = "Binding result on " + Navigation.BILL_MODELS_ADD + " has error(s) for the POST request method.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.BILL_MODELS_ADD);
        }

        // Process the received data
        BillModelsManager billModels = new BillModelsManager();
        billModels.loadBillModelsFromLocalFile();
        // Same counterID can be used only once
        if (billModels.getBillModelUsingCounterID(billModel.getCounterID()) == null) {
            // Add it
            billModels.getBillModels().addLast(billModel);
            billModels.storeBillModelsIntoLocalFile();

            // Log succes
            String message = "Bill model \"" + billModel.getCounterID() + " - " + billModel.getCounterDescription() + "\" was added.";
            Messages.showSuccessMessage(model, message);
            model.addAttribute("added", true);
            // Reset model
            billModel = new BillModel();
        } else {
            // Log Failure
            String message = "There is already a bill model with Counter ID \"" + billModel.getCounterID() + "\".";
            Messages.showErrorMessage(model, message);
        }
        return new ModelAndView(Navigation.BILL_MODELS_ADD, Navigation.BILL_MODELS_ADD, billModel);
    }

    @RequestMapping(value = Navigation.BILL_MODELS_REMOVE, method = RequestMethod.GET)
    public ModelAndView showRemoveConfirmation(@RequestParam("modelCounterID") String modelCounterID, @ModelAttribute(Navigation.BILL_MODELS_REMOVE) BillModel billModel, Model model) {
        // Page Titel
        setTitleOfModelRemove(model);

        // Check received data
        if (modelCounterID == null || modelCounterID.equalsIgnoreCase("")) {
            String message = "Needed parameter was not passed to the " + Navigation.BILL_MODELS_REMOVE;
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.BILL_MODELS_REMOVE);
        }

        // Search BillModel to delete
        BillModelsManager billModelsManager = new BillModelsManager();
        billModelsManager.loadBillModelsFromLocalFile();
        billModel = billModelsManager.getBillModelUsingCounterID(modelCounterID);
        if (billModel == null) {
            String message = "Bill model with Counter ID \"" + modelCounterID + "\" could not be found.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.BILL_MODELS_REMOVE);
        }

        // Model is there, ask for confirmation on the view via showing model information        
        return new ModelAndView(Navigation.BILL_MODELS_REMOVE, Navigation.BILL_MODELS_REMOVE, billModel);
    }

    @RequestMapping(value = Navigation.BILL_MODELS_REMOVE, method = RequestMethod.POST)
    public ModelAndView removeModel(@ModelAttribute(Navigation.BILL_MODELS_REMOVE) BillModel billModel, BindingResult result, Model model) {
        // Page Titel
        setTitleOfModelRemove(model);

        // Check received data
        if (result.hasErrors()) {
            String message = "Binding result on " + Navigation.BILL_MODELS_REMOVE + " has error(s) for the POST request method.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.BILL_MODELS_REMOVE);
        }

        // Process the received data
        BillModelsManager billModelsManager = new BillModelsManager();
        billModelsManager.loadBillModelsFromLocalFile();
        BillModel removeeBillModel = billModelsManager.getBillModelUsingCounterID(billModel.getCounterID());
        // Check if the bill model at least still there
        if (removeeBillModel == null) {
            String message = "Bill model with counterID=" + billModel.getCounterID() + " could not be found.";
            Messages.showErrorMessage(model, message);
            return new ModelAndView(Navigation.BILL_MODELS_REMOVE);
        }
        // It is there and can be removed
        billModelsManager.getBillModels().remove(removeeBillModel);
        billModelsManager.storeBillModelsIntoLocalFile();

        // Log
        String message = "Bill model with counter ID " + removeeBillModel.getCounterID() + " was removed.";
        Messages.showSuccessMessage(model, message);
        model.addAttribute("removed", true);

        // Done, reload and show on the same page
        return new ModelAndView(Navigation.BILL_MODELS_REMOVE, Navigation.BILL_MODELS_REMOVE, removeeBillModel);
    }
}
