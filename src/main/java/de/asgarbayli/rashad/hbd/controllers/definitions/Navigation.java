/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.asgarbayli.rashad.hbd.controllers.definitions;

/**
 *
 * @author rashad
 */
public class Navigation {
    
    /**
     * Forwards to the Parameters Editor page.
     */
    final public static String PARAMETERS = "parameters";
    
    /**
     * Forwards to the About page.
     */
    final public static String ABOUT = "about";
    
    /**
     * Forwards to the Dashboard page.
     */
    final public static String DASHBOARD = "dashboard";
    
    /**
     * Forwards to the page where user can update the last counter for a selected bill model.
     */
    final public static String UPDATE_COUNTER = "update-counter";
    
    /**
     * Forwards to the page where user see and update the counter history for a selected bill model.
     */
    final public static String UPDATE_COUNTER_HISTORY = UPDATE_COUNTER + "-history";
    
    /**
     * Forwards to the BillModels management page.
     */
    final public static String BILL_MODELS = "manage-bill-models";
    
    /**
     * Forwards to the page to add new bill model.
     */
    final public static String BILL_MODELS_ADD = BILL_MODELS + "-add";
    
    /**
     * Forwards to the page to remove the selected bill model.
     */
    final public static String BILL_MODELS_REMOVE = BILL_MODELS + "-remove";
}
