package de.asgarbayli.rashad.hbd.controllers.definitions;

/**
 * Contains available billing types
 * @author rashad
 */
public enum BillingType {

    /**
     * enum type for electricity
     */
    ELECTRICITY,
    
    /**
     * enum type for gas
     */
    GAS
}
