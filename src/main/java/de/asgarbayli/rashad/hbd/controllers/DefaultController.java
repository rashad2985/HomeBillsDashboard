/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.asgarbayli.rashad.hbd.controllers;

import de.asgarbayli.rashad.hbd.controllers.definitions.Navigation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author rashad
 */
@Controller
public class DefaultController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(ModelMap map) {
        return "redirect:" + Navigation.DASHBOARD;
    }
    
    @RequestMapping(value = Navigation.ABOUT, method = RequestMethod.GET)
    public String about(ModelMap map) {
        map.put("title", "About");
        return Navigation.ABOUT;
    }
}
