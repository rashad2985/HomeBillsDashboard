package de.asgarbayli.rashad.hbd.controllers.calculator;

import de.asgarbayli.rashad.hbd.models.BillModel;
import de.asgarbayli.rashad.hbd.models.LocalDateMap;
import de.asgarbayli.rashad.hbd.models.ResultModel;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * General type of billing calculator
 * @author rashad
 */
public class Calculator {
    
    /**
     * Generates a list of result models from a list of bill models via calculating results for each bill model in the list.
     * @param billModels is a <code>LinkedList&lt;BillModels&gt;</code> that contains bill models for calculation.
     * @return a <code>LinkedList&lt;ResultModel&gt;</code> that contains the calculation result for each individual BillModel from the passed list.
     */
    public static LinkedList<ResultModel> generateResultsFrom(LinkedList<BillModel> billModels) {
        LinkedList<ResultModel> resultModels = new LinkedList<>();
        for (BillModel billModel : billModels) {
            ResultModel resultModel = calculateResultFrom(billModel);
            if (resultModel != null) {
                resultModels.addLast(resultModel);
            }
        }
        return resultModels;
    }
    
    private static ResultModel calculateResultFrom(BillModel billModel) {
        ResultModel resultModel = new ResultModel();
        resultModel.setStartDate(billModel.getStartDate());
        // Start date for the result should be the new billing year
        resultModel.getStartDate().plusYears((LocalDate.now().toEpochDay() - resultModel.getStartDate().toEpochDay()) / 365);
        // Sort counters and filter out the non-relevant old counters
        Collections.sort(billModel.getCounterHistory());
        filterOutOldCounters(resultModel.getStartDate(), billModel.getCounterHistory());
        // Get the latest counter (also counter date)
        resultModel.setEndDate(billModel.getCounterHistory().getLast().getKey());
        resultModel.setDays(calculateDays(resultModel.getStartDate(), resultModel.getEndDate()));
        resultModel.setConsume(counterDifference(billModel.getStartCounter(), billModel.getCounterHistory().getLast().getValue()));
        // Check and apply consume factor, if there is one.
        if (billModel.getConsumeFactor() != null && Math.abs(0.0 - billModel.getConsumeFactor()) > 0.000001) {
            resultModel.setConsume(resultModel.getConsume() * billModel.getConsumeFactor());
        }
        // Calculate total bill for the last counter
        resultModel.setTotalNow(calculateTotalNow(resultModel.getConsume(),
                billModel.getUnitPrice(), billModel.getBasePrice(),
                billModel.getTax(), resultModel.getDays()));
        // Calculate expected billing
        resultModel.setTotalExpected(calculateTotalExpected(resultModel.getTotalNow(), resultModel.getDays()));
        //TODO Calculate totalDifference
        //TODO Calculate totalLimit
        //TODO Calculations for diagrams
        return resultModel;
    }
    
    /**
     * Filters out the old counters that don't belong to the new billing year and
     * are not relevant for calculation
     * @param startDate <code>LocalDate</code> is the date of the start of the fresh bill year
     * @param counterHistory <code>LocalDateMap&lt;Integer&gt;</code> history of counters that should be filtered out
     */
    private static void filterOutOldCounters(LocalDate startDate, LinkedList<LocalDateMap<Integer>> counterHistory) {
        if (startDate == null || counterHistory == null) {
            return;
        }
        // The list is already sorted
        while (counterHistory.size() > 1) {
            Iterator<LocalDateMap<Integer>> iterator = counterHistory.iterator();
            // Take and check the first element
            LocalDateMap<Integer> counter = iterator.next();
            LocalDateMap<Integer> next = iterator.next();
            if (next.getKey().toEpochDay() < startDate.toEpochDay()) {
                counterHistory.remove(counter);
            } else {
                return;
            }
        }
    }
    
    /**
     * Calculates the days from start til to the last counter update.
     * If last counter was before the start date, result will be zero.
     * @param lastUpdateDate <code>LocalDate</code> is the date of the last counter update.
     * @param contractDate <code>LocalDate</code> is the date of the start of the fresh bill year.
     * @return <code>Long</code> is the days from start of the fresh bill year
     * til to the last counter update.
     */
    private static Long calculateDays(LocalDate contractDate, LocalDate lastUpdateDate) {
        Long lastDay = lastUpdateDate.toEpochDay();
        Long firstDay = contractDate.toEpochDay();
        Long days;
        if (lastDay < firstDay) {
            days = 0L;
        } else {
            days = lastDay - firstDay;
        }
        return days;
    }
    
    /**
     * Calculates the difference of the start and end counters. The calculation also considers the counter overflow.
     * @param startCounter <code>Interger</code> containing the old counter
     * @param endCounter <code>Interger</code> containing the new counter
     * @return the difference of start and end counters.
     */
    private static Double counterDifference(Integer startCounter, Integer endCounter) {
        Double realDifference;
        if (endCounter >= startCounter) {
            realDifference = (double) (endCounter - startCounter);
        } else {
            realDifference = (double) (endCounter
                    + ((long) Math.pow(10.0, Math.ceil(Math.log10(startCounter))))
                    - startCounter);
        }
        return realDifference;
    }
    
    /**
     * Calculates total billing til to the last counter update, includes also
     * the base price til to the last counter update. Tax is also applied to
     * the end result.
     * @param consume <code>Double</code> is the counter difference that also
     * includes the consume factor application.
     * @param unitPrice <code>Double</code> is the price per unit of counter
     * difference.
     * @param basePrice <code>Double</code> is the base price set by seller
     * and is added to the consume per year.
     * @param tax <code>Double</code> is the tax for the current bill type.
     * Must be in form 0 &lt; xy.ab...c &lt; 100. 
     * @param days <code>Long</code> is the days passed from the contract
     * start til to the last counter update, but less than a whole year.
     * @return <code>Double</code> containing the total bill for the days.
     */
    private static Double calculateTotalNow(Double consume, Double unitPrice, Double basePrice, Double tax, Long days) {
        // First of all, calculate current bill for consume
        Double totalNow = consume * unitPrice;
        // Add part of the base price up to the last counter date
        totalNow += (double) (((double) days) * basePrice / 365.0);
        // Apply tax
        totalNow += totalNow * (tax / 100.0);
        // Return total
        return totalNow;
    }
    
    /**
     * Calculates expected total billing to the end of the billing year, includes also
     * the base price and the tax.
     * @param totalNow <code>Double</code> is the current total billing from start of the new billing year
     * @param days <code>Long</code> is the days passed from the contract
     * @return <code>Double</code> is the expected billing to the end of the billing year,
     * if the consume will be the same as from start of the new billing year until today
     */
    private static Double calculateTotalExpected(Double totalNow, Long days) {
        return (totalNow / ((double) days) * 365.25);
    }
}
