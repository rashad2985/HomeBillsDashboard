/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.asgarbayli.rashad.hbd.controllers.definitions;

import org.springframework.ui.Model;

/**
 * This class handles the messages sent the view model.
 *
 * @author Rashad Asgarbayli
 * @version 1.0
 */
public class Messages {

    /**
     * Resets the message to the empty invisible parameters.
     *
     * @param model is the Model, whose settings are changed.
     */
    public static void resetMessage(Model model) {
        model.addAttribute("display", "none");
        model.addAttribute("messageType", "");
        model.addAttribute("message", "");
    }

    /**
     * Sets the message to the visible error mode and shows the passed message.
     *
     * @param model is the Model, whose settings are changed.
     * @param message is the message string that is showed on the view.
     */
    public static void showErrorMessage(Model model, String message) {
        System.err.println(message);
        model.addAttribute("display", "block");
        model.addAttribute("messageType", "danger");
        model.addAttribute("message", message);
    }

    /**
     * Sets the message to the visible success mode and shows the passed
     * message.
     *
     * @param model is the Model, whose settings are changed.
     * @param message is the message string that is showed on the view.
     */
    public static void showSuccessMessage(Model model, String message) {
        System.out.println(message);
        model.addAttribute("display", "block");
        model.addAttribute("messageType", "success");
        model.addAttribute("message", message);
    }
}
