/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.asgarbayli.rashad.hbd.controllers;

import de.asgarbayli.rashad.hbd.controllers.definitions.Messages;
import de.asgarbayli.rashad.hbd.controllers.definitions.Navigation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author rashad
 */
@Controller
public class DashboardController {
    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("title", "Dashboard");
        Messages.resetMessage(model);
    }
    
    @RequestMapping(value = Navigation.DASHBOARD, method = RequestMethod.GET)
    public ModelAndView showParameters() {
        return new ModelAndView(Navigation.DASHBOARD);
    }

    @RequestMapping(value = Navigation.DASHBOARD, method = RequestMethod.POST)
    public ModelAndView changeParameteres() {
        return new ModelAndView(Navigation.DASHBOARD);
    }
}
