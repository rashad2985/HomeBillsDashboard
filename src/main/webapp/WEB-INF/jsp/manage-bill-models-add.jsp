<%-- 
    Document   : manage-bill-models-add
    Created on : 06-Aug-2017, 15:20:28
    Author     : Rashad Asgarbayli
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="templates/head.jsp" %>
    </head>
    <body>
        <%@include file="templates/navigation.jsp" %>
        <%@include file="templates/header.jsp" %>

        <!-- PARAMETERS -->
        <form:form cssClass="container-fluid" method="POST" action="<%= Navigation.BILL_MODELS_ADD%>" modelAttribute="<%= Navigation.BILL_MODELS_ADD%>">
            <%@include file="templates/message.jsp" %>

            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12 col-md-3">
                                    <div class="form-group">
                                        <label for="counterID">Counter ID:</label>
                                        <form:input cssClass="form-control" path="counterID" id="counterID"/>
                                        <form:errors cssClass="text-danger" path="counterID"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-9">
                                    <div class="form-group">
                                        <label for="counterDescription">Description:</label>
                                        <form:input cssClass="form-control" path="counterDescription" id="counterDescription"/>
                                        <form:errors cssClass="text-danger" path="counterDescription"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="startDate">Contract date:</label>
                                        <form:input cssClass="form-control" path="startDate" id="startDate"/>
                                        <form:errors cssClass="text-danger" path="startDate"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="startCounter">Start counter:</label>
                                        <form:input cssClass="form-control" path="startCounter" id="startCounter"/>
                                        <form:errors cssClass="text-danger" path="startCounter"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="consumeFactor">Consume factor:</label>
                                        <form:input cssClass="form-control" path="consumeFactor" id="consumeFactor"/>
                                        <form:errors cssClass="text-danger" path="consumeFactor"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label class="text-info">Consume factor hint:</label>
                                        <p class="text-info">Hint text</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="unitPrice">Unite price:</label>
                                        <form:input cssClass="form-control" path="unitPrice" id="unitPrice"/>
                                        <form:errors cssClass="text-danger" path="unitPrice"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="basePrice">Annual base price:</label>
                                        <form:input cssClass="form-control" path="basePrice" id="basePrice"/>
                                        <form:errors cssClass="text-danger" path="basePrice"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="monthlyPaymentInAdvance">Monthly payment:</label>
                                        <form:input cssClass="form-control" path="monthlyPaymentInAdvance" id="monthlyPaymentInAdvance"/>
                                        <form:errors cssClass="text-danger" path="monthlyPaymentInAdvance"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="tax">Tax:</label>
                                        <form:input cssClass="form-control" path="tax" id="tax"/>
                                        <form:errors cssClass="text-danger" path="tax"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <a href="/<%=Navigation.BILL_MODELS%>" class="btn btn-default">Back</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <input type="submit" value="Add" class="btn btn-primary"/>
                    </div>
                </div>
            </div>

        </form:form>

        <%@include file="templates/footer.jsp" %>
    </body>
</html>
