<%-- 
    Document   : update
    Created on : 08-Aug-2017, 21:23:35
    Author     : Rashad Asgarbayli
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>

<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="templates/head.jsp" %>
    </head>
    <body>
        <%@include file="templates/navigation.jsp" %>
        <%@include file="templates/header.jsp" %>

        <!-- CALCULATOR -->
        <form:form cssClass="container-fluid" method="POST" action="<%= Navigation.UPDATE_COUNTER %>" modelAttribute="<%= Navigation.UPDATE_COUNTER%>" >
            <%@include file="templates/message.jsp" %>

            <div class="row">
                <div class="col-xs-12">
                    <p>Choose the bill model to add new counter:</p>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="counterID">Select Bill Model:</label>
                        <form:select cssClass="form-control" path="counterID" id="counterID">
                            <c:forEach var="pair" items="${models}">
                                <form:option value="${pair.val0}" label="${pair.val0} - ${pair.val1}" />
                            </c:forEach>
                        </form:select>
                    </div>

                    <div class="form-group">
                        <label for="counterDate">Counter Date:</label>
                        <form:input cssClass="form-control" id="counterDate" path="counterDate"/>
                    </div>

                    <div class="form-group">
                        <label for="counter">Counter:</label>
                        <form:input cssClass="form-control" id="counter" path="counter"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <a href="/<%=Navigation.BILL_MODELS%>" class="btn btn-default">Back</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <input type="submit" value="Add" class="btn btn-primary"/>
                    </div>
                </div>
            </div>

        </form:form>

        <%@include file="templates/footer.jsp" %>
    </body>
</html>