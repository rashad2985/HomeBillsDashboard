<%-- 
    Document   : update-counter-history
    Created on : Aug 10, 2017, 2:57:39 PM
    Author     : Rashad Asgarbayli
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>

<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="templates/head.jsp" %>
    </head>
    <body>
        <%@include file="templates/navigation.jsp" %>
        <%@include file="templates/header.jsp" %>

        <!-- CALCULATOR -->
        <c:set var="navigation" value="<%=Navigation.UPDATE_COUNTER_HISTORY%>" scope="request" />
        <form:form cssClass="container-fluid" method="POST" action="${navigation}" modelAttribute="billModel" >
            <%@include file="templates/message.jsp" %>

            <div class="row">
                <div class="col-xs-12">
                    <ul class="list-group">
                        <li class="list-group-item active">
                            Bill model: ${billModel.counterID} - ${billModel.counterDescription}
                            <form:hidden path="counterID"></form:hidden>
                        </li>
                        <c:forEach var="tuple" items="${billModel.counterHistory}" varStatus="status">
                            <li class="list-group-item">
                                <div class="form-group">
                                    <label for="counterDate${status.index}">Counter Date:</label>
                                    <input name="counterHistory[${status.index}].key" value="${tuple.key}" id="counterDate${status.index}" class="form-control"/>
                                </div>

                                <div class="form-group">
                                    <label for="counter${status.index}">Counter:</label>
                                    <input name="counterHistory[${status.index}].value" value="${tuple.value}" id="counter${status.index}" class="form-control"/>
                                </div>
                            </li>
                        </c:forEach>
                    </ul>

                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <a href="/<%=Navigation.BILL_MODELS%>" class="btn btn-default">Back</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <input type="submit" value="Submit" class="btn btn-primary"/>
                    </div>
                </div>
            </div>

        </form:form>

        <%@include file="templates/footer.jsp" %>
    </body>
</html>