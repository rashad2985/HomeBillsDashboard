<%-- 
    Document   : manage-bill-models
    Created on : 05-Aug-2017, 17:03:11
    Author     : Rashad Asgarbayli
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="templates/head.jsp" %>
    </head>
    <body>
        <%@include file="templates/navigation.jsp" %>
        <%@include file="templates/header.jsp" %>

        <!-- PARAMETERS -->
        <form:form cssClass="container-fluid" method="POST" action="<%= Navigation.BILL_MODELS%>" modelAttribute="billModels">
            <%@include file="templates/message.jsp" %>

            <div class="row">
                <div class="col-xs-12" id="modelList">
                    <c:forEach var="model" items="${billModels.billModels}" varStatus="status">
                        <div class="panel panel-primary" id="${status.index}">
                            <div class="panel-heading" data-toggle="collapse" data-target="#${model.counterID}" style="cursor: pointer;">
                                <div class="row">
                                    <div class="col-xs-12 col-md-3">
                                        <div class="form-group">
                                            <label for="counterID">Counter ID:</label>
                                            <input name="billModels[${status.index}].counterID" value="${model.counterID}" id="counterID" class="form-control" readonly="true"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8">
                                        <div class="form-group">
                                            <label for="counterDescription">Description:</label>
                                            <input name="billModels[${status.index}].counterDescription" value="${model.counterDescription}" id="counterDescription" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-1 text-center">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <a class="btn btn-danger form-control" href="<%= Navigation.BILL_MODELS_REMOVE%>?modelCounterID=${model.counterID}">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="${model.counterID}" class="collapse panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <label for="startDate">Contract date:</label>
                                            <input name="billModels[${status.index}].startDate" value="${model.startDate}" id="startDate" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <label for="startCounter">Start counter:</label>
                                            <input name="billModels[${status.index}].startCounter" value="${model.startCounter}" id="startCounter" class="form-control"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <label for="consumeFactor">Consume factor:</label>
                                            <input name="billModels[${status.index}].consumeFactor" value="${model.consumeFactor}" id="consumeFactor" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <label>Counter history:</label>
                                            <a class="form-control btn btn-primary" href="<%= Navigation.UPDATE_COUNTER_HISTORY%>?modelCounterID=${model.counterID}">Update counter history</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <label for="unitPrice">Unite price:</label>
                                            <input name="billModels[${status.index}].unitPrice" value="${model.unitPrice}" id="unitPrice" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <label for="basePrice">Annual base price:</label>
                                            <input name="billModels[${status.index}].basePrice" value="${model.basePrice}" id="basePrice" class="form-control"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <label for="monthlyPaymentInAdvance">Monthly payment:</label>
                                            <input name="billModels[${status.index}].monthlyPaymentInAdvance" value="${model.monthlyPaymentInAdvance}" id="monthlyPaymentInAdvance" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <label for="tax">Tax:</label>
                                            <input name="billModels[${status.index}].tax" value="${model.tax}" id="tax" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <a href="/<%=Navigation.BILL_MODELS_ADD%>" class="btn btn-default">Add new model</a>
                    </div>
                </div>
                <c:if test="${empty emptyList}">
                    <div class="col-xs-12 col-sm-2">
                        <div class="form-group">
                            <input type="submit" value="Update" class="btn btn-primary"/>
                        </div>
                    </div>
                </c:if>
            </div>

        </form:form>

        <%@include file="templates/footer.jsp" %>
    </body>
</html>
