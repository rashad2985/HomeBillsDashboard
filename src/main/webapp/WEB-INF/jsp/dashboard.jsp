<%-- 
    Document   : dashboard
    Created on : Aug 4, 2017, 3:05:31 PM
    Author     : rashad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="templates/head.jsp" %>
    </head>
    <body>
        <%@include file="templates/navigation.jsp" %>
        <%@include file="templates/header.jsp" %>

        <!-- CONTENT -->
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Panel Heading</div>
                        <div class="panel-body">Panel Content</div>
                        <div class="panel-footer">Panel Footer</div>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="templates/footer.jsp" %>
    </body>
</html>
