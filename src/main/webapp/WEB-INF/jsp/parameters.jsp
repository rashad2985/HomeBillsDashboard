<%-- 
    Document   : parameters
    Created on : Jul 28, 2017, 8:47:56 PM
    Author     : rashad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="templates/head.jsp" %>
    </head>
    <body>
        <%@include file="templates/navigation.jsp" %>
        <%@include file="templates/header.jsp" %>

        <!-- PARAMETERS -->
        <form:form cssClass="container-fluid" method="POST" action="<%= Navigation.PARAMETERS %>" modelAttribute="<%= Navigation.PARAMETERS %>">
            <%@include file="templates/message.jsp" %>
            
            <div class="row">
                <!-- GAS -->
                <div class="col-sm-6 col-lg-4">
                    <h3>Gas</h3>
                    <div class="form-group">
                        <label for="counterGas">Start counter:</label>
                        <form:input id="counterGas" cssClass="form-control" path="counterGas"/>
                    </div>

                    <div class="form-group">
                        <label for="conditionGas">Condition number:</label>
                        <form:input id="conditionGas" cssClass="form-control" path="conditionGas"/>
                    </div>

                    <div class="form-group">
                        <label for="calorificationGas">Calorification value:</label>
                        <form:input id="calorificationGas" cssClass="form-control" path="calorificationGas"/>
                    </div>

                    <div class="form-group">
                        <label for="unitPriceGas">Unite price:</label>
                        <form:input id="unitPriceGas" cssClass="form-control" path="unitPriceGas"/>
                    </div>

                    <div class="form-group">
                        <label for="baseGas">Annual base price:</label>
                        <form:input id="baseGas" cssClass="form-control" path="baseGas"/>
                    </div>

                    <div class="form-group">
                        <label for="billGas">Monthly advance:</label>
                        <form:input id="billGas" cssClass="form-control" path="advanceGas"/>
                    </div>
                </div>

                <!-- ELECTRICITY & CONTRACT DATE -->
                <div class="col-sm-6 col-lg-8">
                    <div class="row">
                        <!-- ELECTRICITY -->
                        <div class="col-lg-6">
                            <h3>Electricity</h3>

                            <div class="form-group">
                                <label for="counterElectricity">Start counter:</label>
                                <form:input id="counterElectricity" cssClass="form-control" path="counterElectricity"/>
                            </div>

                            <div class="form-group">
                                <label for="unitPriceElectricity">Unite price:</label>
                                <form:input id="unitPriceElectricity" cssClass="form-control" path="unitPriceElectricity"/>
                            </div>

                            <div class="form-group">
                                <label for="baseElectricity">Annual base price:</label>
                                <form:input id="baseElectricity" cssClass="form-control" path="baseElectricity"/>
                            </div>

                            <div class="form-group">
                                <label for="billElectricity">Monthly advance:</label>
                                <form:input id="billElectricity" cssClass="form-control" path="advanceElectricity"/>
                            </div>
                        </div>

                        <!-- CONTRACT DATE  & VAT -->
                        <div class="col-lg-6">
                            <h3 class="visible-lg-block visible-xs-block">Common</h3>

                            <div class="form-group">
                                <label for="contractVAT">VAT:</label>
                                <form:input id="contractVAT" cssClass="form-control" path="contractVAT"/>
                            </div>

                            <div class="form-group">
                                <label for="contractDate">Contract date:</label>
                                <form:input id="contractDate" cssClass="form-control" path="contractDate"/>
                            </div>

                            <p>Contract date is common and applied to the both Electricity and Gas calculations.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <input type="submit" value="Submit" class="btn btn-primary"/>
                </div>
            </div>

        </form:form>

        <%@include file="templates/footer.jsp" %>
    </body>
</html>
