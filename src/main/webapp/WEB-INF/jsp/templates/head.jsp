<%-- 
    Document   : thirdparties
    Created on : Jul 28, 2017, 10:29:40 PM
    Author     : Rashad Asgarbayli
--%>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />

<!-- BOOTSTRAP & JQUERY -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- NATIVE -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${cp}/resources/css/site.css" />
<script src="${cp}/resources/js/js.js"></script>
<title>${title}</title>
