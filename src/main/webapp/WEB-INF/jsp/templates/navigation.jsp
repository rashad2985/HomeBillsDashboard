<%-- 
    Document   : navigation
    Created on : Jul 28, 2017, 9:48:18 PM
    Author     : rashad
--%>

<%@page import="de.asgarbayli.rashad.hbd.controllers.definitions.Navigation"%>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </button>
            <a class="navbar-brand" href="http://asgarbayli.de" target="blank"><img class="img-responsive img-brand" src="resources/img/RA.svg" alt="RA"/></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><form:form method="GET" action="<%= Navigation.DASHBOARD %>"><input type="submit" value="Dashboard" class="btn btn-link"/></form:form></li>
                <li><form:form method="GET" action="<%= Navigation.UPDATE_COUNTER %>"><input type="submit" value="Update counters" class="btn btn-link"/></form:form></li>
                <li><form:form method="GET" action="<%= Navigation.BILL_MODELS %>"><input type="submit" value="Manage bills" class="btn btn-link"/></form:form></li>
                <li><form:form method="GET" action="<%= Navigation.ABOUT %>"><input type="submit" value="About" class="btn btn-link"/></form:form></li>
            </ul>
        </div>
    </div>
</nav>