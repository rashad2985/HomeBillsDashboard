<%-- 
    Document   : message
    Created on : 06-Aug-2017, 15:24:57
    Author     : Rashad Asgarbayli
--%>

<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-${messageType}" style="display: ${display};">${message}</div>
    </div>
</div>