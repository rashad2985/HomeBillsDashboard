<%-- 
    Document   : manage-bill-models-remove
    Created on : 06-Aug-2017, 17:27:08
    Author     : rashad
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="templates/head.jsp" %>
    </head>
    <body>
        <%@include file="templates/navigation.jsp" %>
        <%@include file="templates/header.jsp" %>

        <!-- PARAMETERS -->
        <form:form cssClass="container-fluid" method="POST" action="<%= Navigation.BILL_MODELS_REMOVE%>" modelAttribute="<%= Navigation.BILL_MODELS_REMOVE%>">
            <%@include file="templates/message.jsp" %>
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel
                         <c:if test="${empty removed}">panel-primary</c:if>
                         <c:if test="${not empty removed}">panel-danger</c:if>
                         ">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12 col-md-3">
                                    <div class="form-group">
                                        <label for="counterID">Counter ID:</label>
                                        <form:input cssClass="form-control" path="counterID" id="counterID" readonly="true"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-9">
                                    <div class="form-group">
                                        <label for="counterDescription">Description:</label>
                                        <form:input cssClass="form-control" path="counterDescription" id="counterDescription" readonly="true"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="startDate">Contract date:</label>
                                        <form:input cssClass="form-control" path="startDate" id="startDate" readonly="true"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="startCounter">Start counter:</label>
                                        <form:input cssClass="form-control" path="startCounter" id="startCounter" readonly="true"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="consumeFactor">Consume factor:</label>
                                        <form:input cssClass="form-control" path="consumeFactor" id="consumeFactor" readonly="true"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label class="text-info">Consume factor hint:</label>
                                        <p class="text-info">Hint text</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="unitPrice">Unite price:</label>
                                        <form:input cssClass="form-control" path="unitPrice" id="unitPrice" readonly="true"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="basePrice">Annual base price:</label>
                                        <form:input cssClass="form-control" path="basePrice" id="basePrice" readonly="true"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="monthlyPaymentInAdvance">Monthly payment:</label>
                                        <form:input cssClass="form-control" path="monthlyPaymentInAdvance" id="monthlyPaymentInAdvance" readonly="true"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="tax">Tax:</label>
                                        <form:input cssClass="form-control" path="tax" id="tax" readonly="true"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <a href="/<%=Navigation.BILL_MODELS%>" class="btn btn-default">
                            <c:if test="${empty removed}">Cancel</c:if>
                            <c:if test="${not empty removed}">Back</c:if>
                        </a>
                    </div>
                </div>
                <c:if test="${empty removed}">
                    <div class="col-xs-12 col-sm-2">
                        <div class="form-group">
                            <input type="submit" value="Remove" class="btn btn-danger"/>
                        </div>
                    </div>
                </c:if>
            </div>

        </form:form>

        <%@include file="templates/footer.jsp" %>
    </body>
</html>
