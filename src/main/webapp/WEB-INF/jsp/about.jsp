<%-- 
    Document   : about
    Created on : Jul 28, 2017, 8:47:36 PM
    Author     : rashad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="templates/head.jsp" %>
    </head>
    <body>
        <%@include file="templates/navigation.jsp" %>
        <%@include file="templates/header.jsp" %>

        <!-- CONTENT -->
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-xs-6 text-right">Application:</div>
                <div class="col-xs-6 text-left">Home Bills Dashboard</div>
            </div>
            <div class="row text-center">
                <div class="col-xs-6 text-right">Version:</div>
                <div class="col-xs-6 text-left">0.2-beta</div>
            </div>
            <div class="row text-center">
                <div class="col-xs-6 text-right">Author:</div>
                <div class="col-xs-6 text-left">Rashad Asgarbayli</div>
            </div>
            <div class="row text-center">
                <div class="col-xs-6 text-right">Web-site:</div>
                <div class="col-xs-6 text-left"><a href="http://asgarbayli.de" target="blank">http://www.asgarbayli.de</a></div>
            </div>
        </div>

        <%@include file="templates/footer.jsp" %>
    </body>
</html>
