# Home Bills Dashboard
Web based, configurable dashboard that calculates/estimates different home bills.

Calculation is done on a Java back-end (e.g. Tomcat) and results are shown in a browser.

To be able to use this, you need a server that supports Java. Implementation involves Spring Framework.

Just build the project with maven build and deploy it into a server (Tomcat 8+ is recommended). That's it.
